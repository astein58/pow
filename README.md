# Proof Of Work

###LICENSE: BSD-3
###Version: 1.0.0

It's based on a revers SHA512_224 function with a dynamic difficulty.
New generated "POW" are saved in memeory tree using "github.com/google/btree".
Challenge request contains an ID, a target, the needed difficulty and an expiration time.

## Doc
As usua the documentation can be find at [godoc.org](https://godoc.org/gopkg.it/astein58/pow.v1).

## Install
Install the package with:
```
go get gopkg.it/astein58/pow.v1
```

## Server Side
#### Generate a challenge request:
```
challengeRequest, _ := powGetChallenge(2, time.Now()) // build a request with the default expiration time (5 minutes)
```

#### Verify the solution:
```
if !pow.Check(resp) {
	t.Fail()
}
```

## Client Side
#### Resolve the challenge:
```
solutionResponse, _ := pow.Resolve(challengeRequest)
```
