// Package pow is a simple "proof of work" interface.
//
// It's based on a revers SHA512_224 function with a dynamic difficulty.
// New generated "POW" are saved in memeory tree using "github.com/google/btree".
// Challenge request contains an ID, a target, the needed difficulty and
// an expiration time.
//
// The package save issued challenge requests and delete it when validate.
// That way the POW can't be use more than ones. Any request to the tree
// are piped to a channel to permit secured concurent access.
//
// Embedded JSON generation, resolution and check for simple communication.
//
// It is licence under "BSD 3-Clause License"
package pow

import (
	"encoding/json"
	"time"

	"bitbucket.org/astein58/pow/challenger"
	"bitbucket.org/astein58/pow/common"
	"bitbucket.org/astein58/pow/request"
	"bitbucket.org/astein58/pow/store"
)

// Check give an easy way to just check if the response.
// It check the resulting hash is correct and
// the chalenge is part of the actives challenges list.
func Check(resp *challenger.Response) bool {
	diffAsByte := common.GetDiffAsByte(resp.Challenge.Difficulty)

	// convert the ID as number into a slice of bytes
	saltAsBytes := common.IDToSlice(resp.Challenge.ID)

	// Build the attempts objects
	t := &common.TryStruct{
		N:    resp.Solution,
		Salt: saltAsBytes,
	}

	// // Build the function witch try to resolve the challenge
	// hasher := common.NewTryerHasher(saltAsBytes, resp.Challenge.Difficulty)
	// Wayt for a reply of the tryer
	res, _ := common.Hash(t)

	// if the result is not the wanted length it split the extra length
	if uint(len(res)) != diffAsByte {
		res = res[:diffAsByte]
	}

	// Ask the the store to get the corresponding request from RAM
	//
	// Make the action
	action := store.NewAction(store.GetAction, resp.Challenge)
	// Submit the action to be saved in memory
	store.SubmitAction(action)

	// Result from the stor request
	savedReq, ok := <-action.ResponseChan
	// ok tells if the channel is open or not
	// If the challenge ID and the saved request challenge id are not equal
	// or the get returned an error
	if !ok || savedReq.ID != resp.Challenge.ID || savedReq.Err != nil {
		// The response is false
		return false
	}

	// Test if the response is realy good
	return completeCheck(resp.Challenge, resp, res, resp.Challenge.Difficulty)
}

// CheckJSON do the same as check but take a slice of byte as argument.
// The slice must represent a challenger.Response as JSON.
func CheckJSON(input []byte) bool {
	resp := &challenger.Response{}

	jsonMarshalErr := json.Unmarshal(input, resp)
	if jsonMarshalErr != nil {
		return false
	}

	return Check(resp)
}

// GetChallenge return a challenge object or an error
func GetChallenge(difficulty uint16, expirationTime time.Time) (*request.Challenge, error) {
	// Build a new challenge request
	reqChallenge, err := request.BuildNewRequestChalange(difficulty, expirationTime)
	if err != nil {
		return nil, err
	}

	// Make the action
	action := store.NewAction(store.InsertAction, reqChallenge)
	// Submit the action to be saved in memory
	store.SubmitAction(action)

	// Wait for anser
	retChan, ok := <-action.ResponseChan
	// If the channel is dead (ok == false) the insertion have been done
	if !ok {
		// Return the previously made request and no error
		return reqChallenge, nil
	} else if retChan != nil && retChan.Err != nil {
		// If the response has an error it's returned
		return retChan, retChan.Err
	}

	// This should never benn called
	return nil, nil
}

// GetChallengeAsJSON is analog to GetChallenge
// Exept it returns a slice of byte representing the element as JSON
func GetChallengeAsJSON(difficulty uint16, expirationTime time.Time) ([]byte, error) {
	req, reqErr := GetChallenge(difficulty, expirationTime)
	if reqErr != nil {
		return nil, reqErr
	}

	return req.AsJSON(), nil
}

// Resolve true to resolve the challenge. It returns the solution or an error.
func Resolve(challengeRequest *request.Challenge, limits *challenger.ResolveLimits) (*challenger.Response, error) {
	// Do the resolving of the challenge
	return challenger.Resolve(challengeRequest, limits)
}

// ResolveAsJSON is analog to Resolve
// Exept it returns a slice of byte representing the element as JSON
func ResolveAsJSON(input []byte, limits *challenger.ResolveLimits) ([]byte, error) {
	return challenger.ResolveJSON(input, limits)
}

// NewResolveLimits build a new resolver limitations
func NewResolveLimits(dur time.Duration, trys uint64, nbBytes int) *challenger.ResolveLimits {
	return &challenger.ResolveLimits{
		Duration: dur,
		Trys:     trys,
		NbBytes:  nbBytes,
	}
}
