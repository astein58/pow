package challenger

import (
	"encoding/json"
	"fmt"
	"math/big"
	"runtime"
	"time"

	"bitbucket.org/astein58/pow/common"
	"bitbucket.org/astein58/pow/request"
)

// Resolve get a challenge request as paramiter and try to resolve the given request
func Resolve(challenge *request.Challenge, limits *ResolveLimits) (*Response, error) {
	// make a varible to save the stat and the solution if finded
	res := &resolver{
		Found:         false,
		NbRunnningTry: 0,
	}

	if limits == nil {
		limits = &ResolveLimits{
			Duration: time.Minute,
			Trys:     0,
			NbBytes:  0,
		}
	}

	// Make the response channel dedicated to this try
	tryResponseChan := make(chan *common.TryResponse, 1024)

	// Save the solution and returns it
	rep := &Response{
		Solution: []byte{},

		Challenge: challenge,
	}

	// Run the check in an other goroutine
	go checker(res, tryResponseChan, limits)

	// This function is waitting until a solution is found
	rep.Solution = tryer(challenge, res, tryResponseChan)

	if len(rep.Solution) == 0 {
		return nil, fmt.Errorf("The solver reached a limit")
	}

	return rep, nil
}

// ResolveJSON is an alias of Resolve but take a slice of byte insted of the direct struct.
// It return the response as a JSON.
func ResolveJSON(input []byte, limits *ResolveLimits) ([]byte, error) {
	// Init the new container
	challenge := &request.Challenge{}

	// Put data in the container
	jsonMarshalErr := json.Unmarshal(input, challenge)
	if jsonMarshalErr != nil {
		return nil, jsonMarshalErr
	}

	// Do the resolving
	resp, resErr := Resolve(challenge, limits)
	if resErr != nil {
		return nil, resErr
	}

	// Rebuild a JSON object
	asJSON, makeJSONerr := json.Marshal(resp)
	if makeJSONerr != nil {
		return nil, makeJSONerr
	}

	// Return the JSON object
	return asJSON, nil
}

// tryer is a loop waiting for the solution.
// It runs multiple concurent try and stop processing when the solution is found.
//
// This function take the Challenge request, the pointer of the solution for
// this challenge and the channel to answer the resolution.
func tryer(challenge *request.Challenge, resolverObj *resolver, tryChan chan *common.TryResponse) []byte {
	// Convert the ID into a slice of bytes to use as salt
	saltAsBytes := common.IDToSlice(challenge.ID)

	// currentNbig is the solution to true.
	// It start from "zero" and add "one" every loop
	currentNbig := big.NewInt(0)

	// // Build the hasher struct, it will be reused for every try
	// hasher := common.NewTryerHasher(saltAsBytes, challenge.Difficulty)

	// Start the loop
	for {
		// If the solution is found
		if resolverObj.Found {
			// Return the solution
			return resolverObj.Solution
			// If the limit of concurent try is reached
		} else if resolverObj.NbRunnningTry > runtime.NumCPU()*4096 {
			// Sleep for 100 millisecond
			time.Sleep(time.Millisecond * 100)
			// and restart the loop
			continue
		}

		// Convert the possible solution into a slice of bytes
		currentN := currentNbig.Bytes()

		// Build a TryStruct to send to test into the hasher
		t := &common.TryStruct{
			N:         currentN,
			Salt:      saltAsBytes,
			Target:    challenge.ChallengeTarget,
			Challenge: challenge,
		}

		// Run the test for this solution
		go common.Try(t, tryChan)

		// Increment the current try counter
		resolverObj.NbRunnningTry++

		// Add one to the counter do change the next solution to try
		currentNbig.Add(currentNbig, big.NewInt(1))
	}
}

// checker is just a loop waiting for the solution and return it when ready.
func checker(resolverObj *resolver, tryChan chan *common.TryResponse, limits *ResolveLimits) {
	// Init counter
	n := uint64(0)
	// remumber the starting time
	startTime := time.Now()
	for {
		// Get the response from the channel
		tryResponse := <-tryChan

		// Increment the counter
		n++

		// If the response has found
		if tryResponse.Found {
			// Save the solution and the stat and return
			resolverObj.Found = true
			resolverObj.Solution = tryResponse.Solution
			return
		}

		// The tryer reach a limit
		if n >= limits.Trys ||
			startTime.Add(limits.Duration).Before(time.Now()) ||
			len(tryResponse.Solution) >= limits.NbBytes {

			// Set to true and save an empty solution
			resolverObj.Found = true
			resolverObj.Solution = []byte{}
			break
		}

		// If not found decrement the concurents trys counter
		resolverObj.NbRunnningTry--
	}
}

// Response is the element returned by the client to the server to give it's answer
type Response struct {
	Solution []byte // The solution to the challenge

	Challenge *request.Challenge // The challenge refering to
}

// resolver is just a simple element to manage the stats of the trys
// If it's found, how many trys are in processing and the founded solution (if done)
type resolver struct {
	Found         bool
	NbRunnningTry int
	Solution      []byte
}

// ResolveLimits set some limits on the resolution
type ResolveLimits struct {
	Duration time.Duration
	Trys     uint64
	NbBytes  int // represent the limit of byte it can try, if 3 it will stop at [255,255,255] solution
}
