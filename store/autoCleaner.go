package store

import (
	"time"

	"github.com/google/btree"

	"bitbucket.org/astein58/pow/request"
)

var (
	// The element from the channel are element to remove from the challenge list
	autoRmChan = make(chan *RmObj, 1024)
	// This channel is used as internal communication to prevent concurency problem
	appendRmChan = make(chan *RmObj, 1024)
)

// autoRemoveLoop is the initializer of the file.
// It make every variable and the loop witch will handle the RM requests
func autoRemoveLoop() {
	// Init an other loop witch actualy remove the element from the RM list
	go rmLoop()

	// Start the infinity loop, it is locked by the channnel
	for {
		rmReq := <-autoRmChan

		// Check if it's a immediat deletation
		if rmReq.immediat || rmReq.rmTime.Before(time.Now()) {
			// If yes it ask for remove immediately
			rmFunc(rmReq.id)
		} else {
			// Other ways it add it to the waiting list
			appendRmChan <- rmReq
		}
	}
}

// This loop do the remove request to the tree
func rmLoop() {
	// Init the RM waiting list (it is a tree actualy)
	rmTree := btree.New(5)

	// infinity loop
	for {
		// Nonblocking channel select work around
		select {
		// If the channel is loaded it insert the element to the RM waiting list
		case rmElem := <-appendRmChan:
			rmTree.ReplaceOrInsert(*rmElem)
		default:
			// Do nothing to make the channel non blocking
		}

		// rmCounter is will count the numbers of element to remove before the
		// next loop.
		rmCounter := 64

	rmMore:

		// If the list is empty the loop start again
		if rmTree.Len() == 0 {
			time.Sleep(time.Microsecond * 10)
			continue
		}

		// Get the first element to be removed
		// tmpRm, ok := rmTree.Min().(RmObj)
		savedRm := rmTree.Min()
		if savedRm == nil {
			rmTree.DeleteMin()
			goto rmMore
		}
		// If the type convertion fails it delete the min item.
		rmObj, ok := savedRm.(RmObj)
		if !ok {
			rmTree.DeleteMin()
			goto rmMore
		}

		// Check the time the next elements needs to be removed
		// If the time is passed it does the remove immediately
		if rmObj.rmTime.Before(time.Now()) {
			// Decrement the counter
			rmCounter--

			rmTree.DeleteMin()

			// Call the RM function on the tree
			rmFunc(rmObj.id)

			if rmCounter > 0 {
				goto rmMore
			}
		}
	}
}

// Do the remove request to the tree
func rmFunc(id uint64) {
	// Build the element to remove
	elem := request.Challenge{ID: id}

	// Make the action
	action := NewAction(DeleteAction, &elem)
	// Submit the action to be saved in memory
	SubmitAction(action)
}

// RmObj is a struct to transit the remove request betweens go routines
type RmObj struct {
	id       uint64    // The challenge ID
	rmTime   time.Time // The time the element most be removed
	immediat bool      // if the remove needs to be done immediately
}

// Less to implement the btre package
func (r RmObj) Less(thanItem btree.Item) bool {
	than := thanItem.(RmObj)

	if r.rmTime.Before(than.rmTime) {
		return true
	}
	return false
}
