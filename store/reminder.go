package store

import (
	"fmt"

	"bitbucket.org/astein58/pow/request"
	"github.com/google/btree"
)

// Deffine the diferrent type of request
const (
	InsertAction = iota
	GetAction    = iota
	DeleteAction = iota
	HasAction    = iota
)

var (
	// Save the tree in a global package variable
	requestStore *btree.BTree
	// requestChan is the way the tree is edited from multiple go routines
	// without compromising the tree
	requestChan chan *Action
)

func init() {
	// The tree is set with 5 degrees
	requestStore = btree.New(5)
	// Make the communication channel buffered up to 1024 waiting requests
	requestChan = make(chan *Action, 1024)

	// Init the autoremove loop if the challenges are not answered
	go autoRemoveLoop()

	// The loop is needed to catch the tree actions requests
	go waitForAction()
}

// waitForAction is needed to catch the tree actions requests
func waitForAction() {
	// Infinity loop
	for {
		// Get the request
		req := <-requestChan

		// Check the type of request and handle it
		switch req.Action {
		case InsertAction:
			insertInTree(req)
		case GetAction:
			getFromTree(req)
		case DeleteAction:
			deleteFromTree(req)
		case HasAction:
			hasInTree(req)
		}
	}
}

func insertInTree(ac *Action) {
	// At the end of the function the channel has to be closed.
	// The defer key word make it very easy.
	defer close(ac.ResponseChan)

	// Convertion to struct
	structAC := *ac.Challenge
	if requestStore.Has(structAC) {
		ac.ResponseChan <- &request.Challenge{Err: fmt.Errorf("The ID already exist")}
		return
	}

	// Do the actual action on the tree
	requestStore.ReplaceOrInsert(structAC)

	// Remumber to remove the element after expiration time.
	tmpRmObj := RmObj{id: ac.Challenge.ID, rmTime: ac.Challenge.Expire, immediat: false}
	autoRmChan <- &tmpRmObj

	// fmt.Println(requestStore.Len())

}
func getFromTree(ac *Action) {
	// At the end of the function the channel has to be closed.
	// The defer key word make it very easy.
	defer close(ac.ResponseChan)

	// Convertion to struct
	structAC := *ac.Challenge

	// Retreve the saved value
	savedChallenge := requestStore.Get(structAC)

	// If saveedChallenge is nil return error to the channel
	if savedChallenge == nil {
		ac.ResponseChan <- &request.Challenge{Err: fmt.Errorf("ID not find: %q", structAC.ID)}
	}

	// Convert the tree.Item into a *request.Challenge
	retReq, ok := savedChallenge.(request.Challenge)
	if !ok {
		return
	}

	ac.ResponseChan <- &retReq
}
func deleteFromTree(ac *Action) {
	// At the end of the function the channel has to be closed.
	// The defer key word make it very easy.
	defer close(ac.ResponseChan)

	// Convertion to struct
	structAC := *ac.Challenge

	requestStore.Delete(structAC)
}
func hasInTree(ac *Action) {}

// NewAction return a pointer to a Action struct
// Build automaticly the response channel
func NewAction(action int, challenge *request.Challenge) *Action {
	sa := &Action{
		Action:       action,
		Challenge:    challenge,
		ResponseChan: make(chan *request.Challenge, 0),
	}

	return sa
}

// SubmitAction send the given request to the store
func SubmitAction(sub *Action) {
	requestChan <- sub
}

// Action define the action to do associeted with the challenge
// and have a channel to get the response of a get for example
type Action struct {
	Action       int
	Challenge    *request.Challenge
	ResponseChan chan *request.Challenge
}
