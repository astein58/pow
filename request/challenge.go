package request

import (
	"crypto/rand"
	"encoding/json"
	"math/big"
	"time"

	"github.com/google/btree"
)

var (
	// nbByteToFind define the challenge difficulty
	nbByteToFind uint16 = 16

	// Determine how long the challenge is valid
	challengeValidity = time.Minute * 1
)

// BuildNewRequestChalange build a challenge object and save it in memory
// unsing a btree list
func BuildNewRequestChalange(difficulty uint16, expire time.Time) (*Challenge, error) {
	// Build a new random ID (random to not make a incremental ID and is unpredictible)
	newID, randIDerr := rand.Int(rand.Reader, big.NewInt(maxInt64()))
	if randIDerr != nil {
		return nil, randIDerr
	}

	// If no difficulty is set it use the default value
	if difficulty == 0 {
		difficulty = nbByteToFind
	}

	// If the time is set to zero or the expiration date is passed before it is issued.
	// The expiration is the default on.
	if expire.IsZero() || expire.Before(time.Now()) {
		expire = time.Now().Add(challengeValidity)
	}

	// Make a new byte to find (the numbers of byte depends of the difficulty)
	newChalange := make([]byte, getDiffAsByte(difficulty))
	_, randErr := rand.Read(newChalange)
	if randErr != nil {
		return nil, randErr
	}

	// Return the object after been saved in memory
	return &Challenge{
		ID:              newID.Uint64(),
		ChallengeTarget: newChalange,

		Difficulty: difficulty,
		Expire:     expire,
	}, nil
}

// Challenge represent the server challenge
type Challenge struct {
	// It identifys the demand to be sure the request is not reused
	ID uint64
	// A random slice wich must be find by the requester
	ChallengeTarget []byte
	// To have the difficulty in the request to be able
	// to manage different difficultyes (it's in bits)
	Difficulty uint16
	// Expire define the time after what the challenge is set as unvalid (and removed from RAM tree)
	Expire time.Time

	Err error `json:"-"`
}

// Less implement the btree specification
// It is based on the ID (it's a big uint)
func (c Challenge) Less(thanItem btree.Item) bool {
	than := thanItem.(Challenge)

	if c.ID < than.ID {
		return true
	}
	return false
}

// AsJSON just return the slice of byte, it's the JSON representation of the struct.
func (c *Challenge) AsJSON() []byte {
	ret, _ := json.Marshal(c)
	return ret
}

// Functions for convinience
func maxUint64() uint64 {
	return ^uint64(0)
}

func maxInt64() int64 {
	return int64(maxUint64() >> 1)
}

// getDiffAsByte is a copy of the common.GetDiffAsByte function.
// It is distinct because of the cycle import
func getDiffAsByte(diff uint16) uint {
	totalBytes := diff / 8

	if diff%8 == 0 {
		return uint(totalBytes)
	}

	return uint(totalBytes + 1)
}
