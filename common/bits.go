package common

// GetDiffAsByte returns the numbers of bytes needed to hold the this numbers of bits.
func GetDiffAsByte(diff uint16) uint {
	totalBytes := diff / 8

	if diff%8 == 0 {
		return uint(totalBytes)
	}

	return uint(totalBytes + 1)
}

// EqualByte is supposed to be called on the last byte of the check
func EqualByte(b1, b2 byte, diff uint16) bool {
	bitsToCheck := diff % 8
	bb1, bb2 := ByteToBits(b1), ByteToBits(b2)

	for i := 0; i < int(bitsToCheck); i++ {
		if bb1[i] != bb2[i] {
			return false
		}
	}
	return true
}

// ByteToBits convert a byte into a slice of bits
func ByteToBits(x byte) (ret [8]byte) {
	// Loop the to determine the bits
	for i := uint(0); i < 8; i++ {
		ret[i] = x & (1 << i) >> i
	}
	return ret
}

func CheckLastByteEqual(respByte, serverComputing []byte, diff uint16) bool {
	if diff%8 == 0 {
		if respByte[len(respByte)-1] !=
			serverComputing[len(serverComputing)-1] {
			return false
		}
	} else if !EqualByte(
		respByte[len(respByte)-1],
		serverComputing[len(serverComputing)-1], diff,
	) {
		return false
	}
	return true
}
