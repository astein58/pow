// Package common contain the common functions of the challenger and the requester.
//
// This package should'nt be used by users. This functions should be colled from
// the pow package witch calls those functions.
package common

import (
	"crypto/sha512"

	"bitbucket.org/astein58/pow/request"

	"bytes"
	"math/big"
)

// Hash do the hash function
func Hash(t *TryStruct) ([]byte, error) {
	// Build the key
	key := ConcatIDAndTestSolution(t.Salt, t.N)
	res := sha512.Sum512_224(key)
	return res[:], nil
}

// Try try the given solution and return the result into the given channel
func Try(t *TryStruct, tryChan chan *TryResponse) {
	diffAsByte := GetDiffAsByte(t.Challenge.Difficulty)

	// Hash the solution
	res, _ := Hash(t)
	// Keep only the wanted bytes
	res = res[:diffAsByte]

	// Clean the unused part of the slice if not the right length
	if uint(len(res)) != diffAsByte {
		res = res[:diffAsByte]
	}

	// If the difficulty is equal to one byte
	if diffAsByte == 1 {
		// Check if the unique byte is "equal" at the bits level and the difficulty
		if CheckLastByteEqual(res, t.Target, t.Challenge.Difficulty) {
			tryChan <- &TryResponse{
				Found:    true,
				Solution: t.N,
			}
			return
		}

		// If the difficulty is bigger than one byte
	} else if diffAsByte > 1 {
		// If the target is the same as the result.
		if bytes.Equal(
			res[:len(res)-1], t.Target[:len(t.Target)-1],
		) {
			// Check the last byte because the firsts are correct
			if CheckLastByteEqual(res, t.Target, t.Challenge.Difficulty) {
				tryChan <- &TryResponse{
					Found:    true,
					Solution: t.N,
				}
				return
			}
		}
	}

	// Otherways it return "Found:False"
	tryChan <- &TryResponse{Found: false, Solution: t.N}
}

// TryStruct is a struct used to define what paramiters must be used for this try.
type TryStruct struct {
	N, Salt, Target []byte
	ResponseChan    chan *TryResponse

	Challenge *request.Challenge
}

// TryResponse represent the response of the try.
type TryResponse struct {
	Found    bool   // Been found
	Solution []byte // Save the solution
}

// IDToSlice is a simple function witch get an integer and return a slice of bytes.
func IDToSlice(id uint64) []byte {
	idAsBigInt := big.NewInt(int64(id))
	return idAsBigInt.Bytes()
}

// ConcatIDAndTestSolution make a string containing the challenge ID and the try.
// It make the try unique for any challenge
func ConcatIDAndTestSolution(id, try []byte) []byte {
	id = append(id, try...)
	return id
}
