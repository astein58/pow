package pow

import (
	"testing"

	"bitbucket.org/astein58/pow/challenger"
	"bitbucket.org/astein58/pow/request"

	"time"
)

var (
	templateChallenge = &request.Challenge{
		ID:              389547449958750524,
		ChallengeTarget: []byte{149},
		Difficulty:      8,
	}
	templateResponse = &challenger.Response{
		Solution:  []byte{2, 159, 168},
		Challenge: templateChallenge,
	}
)

func init() {
	// Give the time to the librery to init the needed functions
	time.Sleep(time.Millisecond * 100)
}

func TestPow(t *testing.T) {
	// Do a challenge request
	challengeRequest, _ := GetChallenge(17, time.Now())

	// // Do the resolving of the challenge
	resp, err := Resolve(challengeRequest, nil)
	if err != nil {
		t.Fail()
	}

	// Verify if the resonse is correct
	if !Check(resp) {
		t.Fail()
	}

	// Verify a second use can't be done
	if Check(resp) {
		t.Fail()
	}
}

func TestPowJSON(t *testing.T) {
	// Do a challenge request
	challengeRequest, _ := GetChallengeAsJSON(17, time.Now())

	// // Do the resolving of the challenge
	resp, err := ResolveAsJSON(challengeRequest, nil)
	if err != nil {
		t.Fail()
	}

	// Verify if the resonse is correct
	if !CheckJSON(resp) {
		t.Fail()
	}

	// Verify a second use can't be done
	if CheckJSON(resp) {
		t.Fail()
	}
}

// TestPowNotValid must be unvalid because the ID is not registered in the store
func TestPowNotValid(t *testing.T) {
	// Verify if the resonse is correct
	if Check(templateResponse) {
		// If the response is correct, the test fails
		t.Fail()
	}
}

func TestDev(t *testing.T) { // Do a challenge request
	challengeRequest, _ := GetChallenge(16, time.Now())

	resolveLimits := NewResolveLimits(time.Minute*100, 18446744073709551615, 3)

	// // Do the resolving of the challenge
	resp, err := Resolve(challengeRequest, resolveLimits)
	if err != nil {
		t.Fail()
		return
	}

	// Verify if the resonse is correct
	if !Check(resp) {
		t.Fail()
		return
	}
}

// BenchmarkPowGenerate try to bensh the speed to generated proof of work request.
func BenchmarkPowGenerate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetChallenge(32, time.Now().Add(time.Second))
	}
}

// BenchmarkPowResolve try to bensh the speed to generated proof of work request.
func BenchmarkPowResolve(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Resolve(templateChallenge, nil)
	}
}
