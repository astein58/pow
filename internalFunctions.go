package pow

import (
	"bytes"
	"time"

	"bitbucket.org/astein58/pow/challenger"
	"bitbucket.org/astein58/pow/common"
	"bitbucket.org/astein58/pow/request"
	"bitbucket.org/astein58/pow/store"
)

// Delete the given element from RAM store
func deleteSavedElem(req *request.Challenge) {
	// Make the action
	action := store.NewAction(store.DeleteAction, req)
	// Submit the action to be saved in memory
	store.SubmitAction(action)
}

// completeCheck do the full check between saved request and response.
func completeCheck(chal *request.Challenge, resp *challenger.Response, serverComputing []byte, diff uint16) bool {
	// Save the difficulty as bytes
	diffAsByte := common.GetDiffAsByte(diff)

	// If the difficulty is equal to one byte
	if diffAsByte == 1 {
		// Check if the unique byte is "equal" at the bits level and the difficulty
		if !common.CheckLastByteEqual(resp.Challenge.ChallengeTarget, serverComputing, diff) {
			return false
		}

		// If the difficulty is bigger than one byte
	} else if diffAsByte > 1 {
		// If the target is the same as the result.
		if bytes.Equal(
			resp.Challenge.ChallengeTarget[:len(resp.Challenge.ChallengeTarget)-1], serverComputing[:len(serverComputing)-1],
		) {
			// Check the last byte because the firsts are correct
			if !common.CheckLastByteEqual(resp.Challenge.ChallengeTarget, serverComputing, diff) {
				return false
			}
		} else {
			return false
		}
	}

	if chal.ID != resp.Challenge.ID {
		return false
	}
	if chal.Expire != resp.Challenge.Expire || chal.Expire.Before(time.Now()) {
		return false
	}

	deleteSavedElem(chal)
	return true
}
